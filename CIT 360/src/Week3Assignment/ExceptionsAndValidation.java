package Week3Assignment;

import java.util.Scanner;

public class ExceptionsAndValidation {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("\nThis program will ask you for 2 numbers and will divide them for you and" +
                "\nprovide the answer as the output.");

        double num1;
        double num2;
        double divide;
        boolean valid = false;

        while (!valid) {
            try {
                System.out.println("What is the first number?");
                num1 = input.nextDouble();
                System.out.println("What is the second number?");
                num2 = input.nextDouble();
                divide = num1 / num2;
                valid = true;
                System.out.println("Your answer is: " + divide);

            } catch (ArithmeticException e) {
                System.out.println("The 2nd number can't be 0. Try Again!");
            }
        }
    }
}
