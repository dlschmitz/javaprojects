package JSONandHTTP;

import java.net.*;
import java.io.*;
import java.util.Map;

public class HTTP {

    //Takes the URL as a string parameter and set up a connection
    //It will read from the connection then goes through the while loop
    //Once it is done it will put it into a string then returns it.

    public static String getHttpContent (String string) {

        String content = "";

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line= reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }

            content = stringBuilder.toString();

        } catch (Exception e) {
            System.err.println(e.toString());
        }

        return content;
    }

    //This is a method that will get us the Headers from HTTP

    public static Map getHttpHeaders (String string) {

        Map hashmap = null;

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            hashmap = http.getHeaderFields();

        } catch (Exception e) {
            System.err.println(e.toString());
        }

        return hashmap;
    }

    public static void main(String[] args) {
        System.out.println(HTTP.getHttpContent( "http://www.google.com"));

        Map<Integer, String> m = HTTP.getHttpHeaders("http://www.google.com");

                for (Map.Entry<Integer, String> entry : m.entrySet()) {
                    System.out.println("Key: " + entry.getKey());
                }
    }
}
