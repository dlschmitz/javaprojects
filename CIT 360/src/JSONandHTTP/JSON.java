package JSONandHTTP;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.IOException;

public class JSON {

    public static String customerToJSON (Customer customer) {
        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(customer);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return s;
    }

    public static Customer JSONToCustomer(String s) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        Customer customer = null;

        try {
            customer = mapper.readValue(s, Customer.class);
        }catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return customer;
    }
//This allows us to add information to the JSON file.
// Currently from the file only 2 objects have been added.

    public static void main(String[] args) throws IOException {

        Customer cust = new Customer();
        cust.setName("Daniel");
        cust.setPhone(3261734);

        String json = JSON.customerToJSON(cust);
        System.out.println(json);

        Customer cust2 = JSON.JSONToCustomer(json);
        System.out.println(cust2);
    }

}
