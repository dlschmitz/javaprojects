package JavaCollections;

import java.util.*;

public class Collections {
    public static void main(String[] args) {
        System.out.println("---List---");
        List reminder = new ArrayList();
        reminder.add("Don't");
        reminder.add("forget");
        reminder.add("to");
        reminder.add("vote");
        reminder.add("before");
        reminder.add("it's");
        reminder.add("too");
        reminder.add("Late!");

        for (Object str:reminder){
            System.out.println((String) str);
        }
        System.out.println("---Set---");
        Set set = new TreeSet();
        set.add("Don't");
        set.add("forget");
        set.add("to");
        set.add("vote");
        set.add("before");
        set.add("it's");
        set.add("too");
        set.add("Late!");

        for (Object str: set){
            System.out.println((String) str);
        }

        System.out.println("---Queue---");
        Queue queue = new PriorityQueue();
        queue.add("Don't");
        queue.add("forget");
        queue.add("to");
        queue.add("vote");
        queue.add("before");
        queue.add("it's");
        queue.add("too");
        queue.add("Late!");

        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());
        }

        System.out.println("---Map---");
        Map map = new HashMap();
        map.put(1,"Don't");
        map.put(2,"forget");
        map.put(3,"to");
        map.put(4,"vote");
        map.put(5,"before");
        map.put(6,"it's");
        map.put(7,"too");
        map.put(6,"Late!");

        for (int i =1; i<8; i++) {
            String result = (String)map.get(i);
            System.out.println(result);
        }

        System.out.println("---List using Marvel Movies in 2019---");
        List<Marvel> myList = new LinkedList<Marvel>();
        myList.add(new Marvel( "Spider-Man: Far From Home", 2019));
        myList.add(new Marvel("Avengers: Endgame", 2019));
        myList.add(new Marvel("Captain Marvel", 2019));

        for (Marvel movies:myList) {
            System.out.println(movies);
        }
    }
}
