package JavaCollections;

public class Marvel {
    private String title;
    private Integer year;

    public Marvel(String title, Integer year) {
        this.title = title;
        this.year = year;
    }

    public String toString() {
        return "Title: " + title + " Year: " + year;
    }
}
